# gentoo-chroot

A fork of `arch-chroot` with modifications to make quickly switching to various gentoo chroots trivial.

Many thanks to the arch folks for providing a good base.

## Usage

```bash
~/scripts/gentoo-chroot.sh «options» ./gentoo-musl-chroot
```

Options:

| **Short Flag** |   **Long Flag**    | **Environment Variable**  |                                                       **Description**                                                        |
| -------------- | ------------------ | :-----------------------: | ---------------------------------------------------------------------------------------------------------------------------- |
| `-b`           | `--bind-binpkgs`   |      `MOUNT_BINPKGS`      | If set and `var/cache/binpkgs` exists on the host, bind mount it in the chroot                                               |
| `-d`           | `--debug`          |          `DEBUG`          | Debug output                                                                                                                 |
| `-k`           | `--no-bind-kernel` | `NO_MOUNT_KERNEL_SOURCE`  | Don't mount `/usr/src/linux`                                                                                                 |
| `-m`           | `--bind-makeconf`  |     `MOUNT_MAKECONF`      | Mount system's `make.conf` into the chroot                                                                                   |
| `-n`           | `--no-bashrc`      |     `NO_MOUNT_BASHRC`     | Don't mount `${SCRIPT_DIR}/bashrc` into `/root`                                                                              |
| `-N`           | `--unshare`        |            N/A            | Run in unshare mode as a regular user                                                                                        |
| `-r`           | `--bind-hostrepos` |     `MOUNT_HOSTREPOS`     | Instead of mounting `/var/db/repos/gentoo` mount `/var/db/repos` and `/etc/portage/repos.conf`                               |
| `-u`           | `--user`           |        `USERSPEC`         | Specify a non-root user and (optional) group to use `user:[group]`                                                           |
| N/A            | N/A                | `ADDITIONAL_CHROOT_FILES` | If this array of absolute file paths exists `( "/path/src/file[:/path/dst]" )` the files will be mounted during chroot setup |
| N/A            | N/A                |        `FILES_DIR`        | Used for mounting `$(pwd)/bashrc`; override for different envs/settings                                                      |
