#!/usr/bin/env bash

# Source this file and run `packagebuild-diff <category/package or category> <ebuild phase>`
# and it will run `ebuild ... clean <phase>` on the latest version of whatever you've pointed it at.
# It'll also build binpkgs of deps and clean them up. Disk space estimates for all of ::gentoo range up to ~120GB

# A sane list of categories to iterate over:
# find /var/db/repos/gentoo -maxdepth 1 -mindepth 1 -type d -not -name ".git" -not -name profiles -not -name "*virtual*" -not -name "*acct*" | sed -e 's /var/db/repos/gentoo/  '

# You probably want to use a portage bashrc file to do something useful with these ebuilds while this is happening

function packagebuild-diff() {
        PHASE=${2:-configure}
        if grep -q "/" <<< $1; then
                PACKAGES=( "$1" )
                echo "Targeting package $1" | tee -a ~/dodgy-progress
        else
                readarray -d '' PACKAGES < <( find /var/db/repos/gentoo/$1 -maxdepth 1 -mindepth 1 -type d -print0 )
                echo "Targeting packages: ${PACKAGES[*]#'/var/db/repos/gentoo/'}" | tee -a ~/dodgy-progress
        fi

        for PACKAGE in ${PACKAGES[@]}; do
                PACKAGE=${PACKAGE#"/var/db/repos/gentoo/"}
                echo "$(date) -> ${PACKAGE}" | tee -a ~/dodgy-progress
                emerge -v1o --binpkg-respect-use=y --getbinpkg=y --buildpkg ${PACKAGE}
                local EBUILD=$(find /var/db/repos/gentoo/${PACKAGE} -maxdepth 1 -name '*ebuild*' -not -name '*9999*' | sort -r | head -1)
                echo "$(date) -> selected ebuild: ${EBUILD}" | tee -a ~/dodgy-progress
                CC="clang" CXX="clang" ebuild ${EBUILD} clean ${PHASE}
                emerge --depclean
        done
}
